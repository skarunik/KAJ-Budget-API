# frozen_string_literal: true

# require 'jwt'

class JsonWebToken
  class << self
    def encode(payload)
      payload.reverse_merge!(meta)
      JWT.encode(payload, Rails.application.credentials.secret_key_base)
    rescue JWT::EncodeError => e
      raise ErrorHandler::EncodeError, "JWT: #{e.message}"
    end

    def decode(token)
      JWT.decode(token, Rails.application.credentials.secret_key_base)
    rescue JWT::DecodeError => e
      raise ErrorHandler::DecodeError, "JWT: #{e.message}"
    end

    def valid_payload?(payload)
      if expired(payload) || payload['iss'] != meta[:iss] || payload['aud'] != meta[:aud]
        false
      else
        true
      end
    end

    def meta
      {
        exp: 24.hours.from_now.to_i,
        iss: 'issuer_name',
        aud: 'user'
      }
    end

    def expired(payload)
      Time.zone.at(payload['exp']) < Time.zone.now
    end
  end
end
