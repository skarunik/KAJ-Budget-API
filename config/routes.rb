# frozen_string_literal: true

Rails.application.routes.draw do
  root 'api/v1/home#index'

  namespace :api do
    namespace :v1 do
      resources :users, only: :create do
        collection do
          post :login
        end
      end

      resources :budgets, except: %i[new edit], shallow: true do
        resources :budget_items, except: %i[new edit]
      end

      resources :payments, except: %i[new edit]
    end
  end
end
