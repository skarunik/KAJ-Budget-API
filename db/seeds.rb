# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database
# with its default values. The data can then be loaded with the rails db:seed
# command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts 'Creating user...'
user = User.find_or_initialize_by(email: 'john_dow@example.com')
user.name =  'John Dow'
user.password = '1qaz2wsx'
user.password_confirmation = '1qaz2wsx'
user.save

user.budgets.delete_all

puts 'Creating budgets...'
budget1 = user.budgets.create!(name: 'IT Budget', description: 'Budget of IT department', user: user)
budget2 = user.budgets.create!(name: 'HR Budget', description: 'Budget of HR department', user: user)

puts 'Creating items...'
10.times do |i|
  budget1.budget_items.create(name: "budget item #{i}", user: user)
  budget2.budget_items.create(name: "budget item #{i}", user: user)
end

puts 'Creating payments...'
budget1.budget_items.each do |bi|
  20.times do |i|
    bi.payments.create(
      amount: rand(10..100),
      user: user,
      date: rand(30.days.ago..Time.zone.now),
      description: "payment #{i}"
    )
  end
end

budget2.budget_items.each do |bi|
  20.times do |i|
    bi.payments.create(
      amount: rand(10..100),
      user: user,
      date: rand(30.days.ago..Time.zone.now),
      description: "payment #{i}"
    )
  end
end
