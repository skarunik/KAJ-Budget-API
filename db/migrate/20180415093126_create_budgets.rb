# frozen_string_literal: true

class CreateBudgets < ActiveRecord::Migration[5.2]
  def change
    create_table :budgets do |t|
      t.string :name, index: true
      t.text :description

      t.timestamps
    end
  end
end
