# frozen_string_literal: true

class AddUserToBudgetItems < ActiveRecord::Migration[5.2]
  def change
    add_reference :budget_items, :user
  end
end
