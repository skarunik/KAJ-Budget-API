class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :amount
      t.string :description
      t.references :budget_item

      t.timestamps
    end
  end
end
