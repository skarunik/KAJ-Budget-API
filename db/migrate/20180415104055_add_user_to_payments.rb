# frozen_string_literal: true

class AddUserToPayments < ActiveRecord::Migration[5.2]
  def change
    add_reference :payments, :user
  end
end
