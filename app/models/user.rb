# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  has_many :budgets, dependent: :destroy
  has_many :budget_items, dependent: :destroy
  has_many :payments, dependent: :destroy

  validates :name, :email, :password_digest, presence: true
  validates :email, uniqueness: true, case_sensitive: false
  validates :email, format: /@/

  before_save :downcase_email

  private

  def downcase_email
    self.email = email.delete(' ').downcase
  end
end
