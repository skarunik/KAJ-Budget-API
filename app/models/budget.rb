# frozen_string_literal: true

# == Schema Information
#
# Table name: budgets
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#
# Indexes
#
#  index_budgets_on_name     (name)
#  index_budgets_on_user_id  (user_id)
#

class Budget < ApplicationRecord
  belongs_to :user
  has_many :budget_items, dependent: :destroy

  validates :name, presence: true, uniqueness: true
  validates :description, length: { maximum: 250 }, allow_blank: true
end
