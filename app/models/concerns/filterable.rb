# frozen_string_literal: true

module Filterable
  extend ActiveSupport::Concern

  module ClassMethods
    def filter(filtering_params)
      results = where(nil)
      filtering_params&.each do |key, value|
        results = results.public_send(key, value) if results && value.present?
      end
      results
    end
  end
end
