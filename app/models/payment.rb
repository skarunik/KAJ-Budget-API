# frozen_string_literal: true

# == Schema Information
#
# Table name: payments
#
#  id             :integer          not null, primary key
#  amount         :integer
#  description    :string
#  budget_item_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  date           :datetime
#
# Indexes
#
#  index_payments_on_budget_item_id  (budget_item_id)
#  index_payments_on_user_id         (user_id)
#

class Payment < ApplicationRecord
  include Filterable

  belongs_to :user
  belongs_to :budget_item

  validates :amount, presence: true
  validates :date, presence: true
  validates :description, length: { maximum: 150 }, allow_blank: true

  scope :budget_item, ->(id) { where(budget_item_id: id) }

  def self.budget(id = nil)
    return all.all unless id
    ids = BudgetItem.where(budget_id: id)&.pluck(:id)
    where(budget_item_id: ids)
  end
end
