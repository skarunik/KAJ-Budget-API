# frozen_string_literal: true

# == Schema Information
#
# Table name: budget_items
#
#  id         :integer          not null, primary key
#  name       :string
#  budget_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_budget_items_on_budget_id  (budget_id)
#  index_budget_items_on_user_id    (user_id)
#

class BudgetItem < ApplicationRecord
  belongs_to :user
  belongs_to :budget
  has_many :payments, dependent: :destroy

  validates :name, presence: true, uniqueness: { scope: :budget }
end
