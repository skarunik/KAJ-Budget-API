# frozen_string_literal: true

module ErrorHandler
  extend ActiveSupport::Concern

  class AuthenticationError < StandardError; end
  class EncodeError < StandardError; end
  class MissingToken < StandardError; end
  class DecodeError < StandardError; end

  included do
    rescue_from Exception, with: :handle_exceptions
  end

  def handle_exceptions(exception)
    case exception
    when ActiveRecord::RecordInvalid then record_invalid(exception)
    when ActiveRecord::RecordNotUnique then record_not_unique(exception)
    when ActiveRecord::RecordNotFound then record_not_found(exception)
    when ErrorHandler::MissingToken then missing_token(exception)
    when ErrorHandler::DecodeError then decode_error(exception)
    when ErrorHandler::AuthenticationError then unauthorized(exception)
    else unprocessable_entity(exception)
    end
  end

  protected

  def record_invalid(e)
    render_error(e.message, :bad_request)
  end

  def record_not_uniq(e)
    render_error(e.message, :conflict)
  end

  def record_not_found(e)
    render_error(e.message, :not_found)
  end

  def missing_token(e)
    render_error(e.message, :not_found)
  end

  def decode_error(e)
    render_error(e.message, :bad_request)
  end

  def unauthorized(e)
    render_error(e.message, :unauthorized)
  end

  def unprocessable_entity(e)
    render_error(e.message, :unprocessable_entity)
  end

  def render_error(message, status = :ok)
    status_code = Rack::Utils::SYMBOL_TO_STATUS_CODE[status]
    render json: { error: { status: status_code, message: message } }, status: status
  end
end
