# frozen_string_literal: true

require 'json_web_token'

module Authenticator
  extend ActiveSupport::Concern

  included do
    before_action :authenticate
  end

  attr_reader :current_user

  protected

  def authenticate
    invalid_authentication unless payload || JsonWebToken.valid_payload?(payload.first)
    load_current_user!
    invalid_authentication unless current_user
  end

  def invalid_authentication
    raise ErrorHandler::AuthenticationError, 'Authentication failed'
  end

  private

  def payload
    auth_header = request.headers['Authorization']
    token = auth_header&.split(' ')&.last
    raise ErrorHandler::MissingToken, 'JWT: token is missing' unless token
    JsonWebToken.decode(token)
  end

  def load_current_user!
    @current_user = User.find_by(id: payload[0]['user_id'])
  end
end
