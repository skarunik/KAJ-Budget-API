# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ErrorHandler
  include Authenticator

  def json_response(object, status = :ok)
    render json: object, status: status
  end
end
