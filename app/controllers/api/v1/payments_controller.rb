# frozen_string_literal: true

module Api
  module V1
    class PaymentsController < ::ApplicationController
      before_action :set_payment, only: %i[show update destroy]
      before_action :set_budget_item, only: %i[create]

      # GET /api/v1/payments
      # GET /api/v1/payments?budget=1&budget_item=3
      def index
        payments = Payment.filter(filter_params)
        json_response(payments)
      end

      def show
        json_response(@payment)
      end

      def create
        payment = current_user.payments.create!(payment_params.merge(budget_item: @budget_item))
        json_response(payment)
      end

      def update
        @payment.update!(payment_params)
        json_response(@payment)
      end

      def destroy
        @payment.destroy
        head :no_content
      end

      private

      def set_payment
        @payment = current_user.payments.find(params[:id])
      end

      def filter_params
        params.permit(:budget, :budget_item)
      end

      def set_budget_item
        @budget_item = current_user.budget_items.find(params[:payment].try(:[], :budget_item_id))
      end

      def payment_params
        params.require(:payment).permit(:amount, :description, :date)
      end
    end
  end
end
