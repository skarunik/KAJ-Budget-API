# frozen_string_literal: true

module Api
  module V1
    class UsersController < ::ApplicationController
      skip_before_action :authenticate, only: %i[create login]

      def create
        User.create!(user_params)
        json_response({ status: 'User created successfully' }, :created)
      end

      def login
        user = User.find_by(email: params[:email].to_s.downcase)

        if user&.authenticate(params[:password])
          auth_token = JsonWebToken.encode(user_id: user.id)
          json_response(auth_token: auth_token)
        else
          json_response({ error: 'Invalid username / password' }, :unauthorized)
        end
      end

      private

      def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      end
    end
  end
end
