# frozen_string_literal: true

module Api
  module V1
    class HomeController < ::ApplicationController
      skip_before_action :authenticate

      def index
        json_response(message: 'Welcome to budget app')
      end
    end
  end
end
