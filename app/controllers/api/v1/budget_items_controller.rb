# frozen_string_literal: true

module Api
  module V1
    class BudgetItemsController < ::ApplicationController
      before_action :set_budget, only: %i[index create]
      before_action :set_budget_item, only: %i[show update destroy]

      def index
        budget_items = @budget.budget_items
        json_response(budget_items)
      end

      def show
        json_response(@budget_item)
      end

      def create
        budget_item = @budget.budget_items.create!(budget_item_params.merge(user: current_user))
        json_response(budget_item)
      end

      def update
        @budget_item.update!(budget_item_params)
        json_response(@budget_item)
      end

      def destroy
        @budget_item.destroy
        head :no_content
      end

      private

      def set_budget
        @budget = current_user.budgets.find(params[:budget_id])
      end

      def set_budget_item
        @budget_item = current_user.budget_items.find(params[:id])
      end

      def budget_item_params
        params.require(:budget_item).permit(:name)
      end
    end
  end
end
