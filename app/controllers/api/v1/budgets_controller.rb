# frozen_string_literal: true

module Api
  module V1
    class BudgetsController < ::ApplicationController
      before_action :set_budget, only: %i[show update destroy]

      def index
        budgets = current_user.budgets
        json_response(budgets)
      end

      def show
        json_response(@budget.as_json(include: :budget_items))
      end

      def create
        budget = current_user.budgets.create!(budget_params)
        json_response(budget)
      end

      def update
        @budget.update!(budget_params)
        json_response(@budget)
      end

      def destroy
        @budget.destroy
        head :no_content
      end

      private

      def set_budget
        @budget = current_user.budgets.find(params[:id])
      end

      def budget_params
        params.require(:budget).permit(:name, :description)
      end
    end
  end
end
